module.exports = function (grunt) {

    grunt
        .initConfig({
            pkg: grunt.file.readJSON('package.json'),
            ngtemplates: {
                nnUtils: {
                    src: 'src/**/*.html',
                    dest: 'htmlcache/templates.js'
                }
            },
            concat: {
                components: {
                    src: ['src/*.js',
                        'src/formValidation/*.js',
                        'src/mobileNumberFormatter/*.js',
                        'htmlcache/templates.js',
                        'src/thermo-meter/*.js'],
                    dest: 'dist/nn-utils.js',
                },
                cssfiles: {
                    src: ['src/**/*.css'],
                    dest: 'dist/nn-utils.css',
                }
            },
            uglify: {
                my_target: {
                    files: {
                        'dist/nn-utils.min.js': [
                            'dist/nn-utils.js'],
                    }
                }
            },
            clean: ['htmlcache']
        });

    grunt.loadNpmTasks('grunt-angular-templates');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.registerTask('default', ['ngtemplates', 'concat', 'uglify', 'clean']);

};