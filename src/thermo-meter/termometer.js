(function () {
angular.module('nnUtils').directive('thermometer', ['$window', function($window) {

  return {
    restrict: 'E',
    scope: true,
    require: 'ngModel',
    templateUrl: 'src/thermo-meter/Termometer.html',
    link: function(scope, elem, attrs, ngModelCtrl) {

      scope.size = attrs.size;
      scope.height = attrs.height;
      scope.percent = attrs.percent;
      scope.ngModelCtrl = ngModelCtrl;
      scope.unit = attrs.unit;
      scope.minVal = attrs.minVal;
      scope.maxVal = attrs.maxVal;

      scope.mainStyle = {height: scope.height + "px"};
      scope.maskStype = {height: (scope.height - 53) + "px"};
      
      scope.refreshMercury = function(){
        scope.mercuryStyle = {height: scope.percent + "%"};
      }
      
      scope.calculatePercentage = function(min, max, value) {
        if (min < 0) {
          var delta = 0 - min;
          min += delta;
          max += delta;
          value += delta;
        }
        scope.p75 = (max * 75) / 100;
        scope.p50 = (max * 50) / 100;
        scope.p25 = (max * 25) / 100;
        
        return (100 * value) / max;
      }

      scope.$watch(function() {
        return ngModelCtrl.$viewValue;
      }, function(newVal, oldVal) {
        if (newVal === scope.percent) { return; }
        if (!scope.minVal && !scope.maxVal) {
          scope.minVal = 0;
          scope.maxVal = 100;
          scope.percent = newVal;
        }
        scope.percent = scope.calculatePercentage(parseFloat(scope.minVal), parseFloat(scope.maxVal), parseFloat(newVal));
        scope.displayVal = newVal;
        scope.refreshMercury();
      })
    }
  };

}]);
})();