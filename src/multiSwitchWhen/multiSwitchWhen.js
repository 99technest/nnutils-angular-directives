/**
 * Created by Sandeep on 2/13/2017. Inspired from below example.
 * http://plnkr.co/edit/K9znnnFiVnKAgGccSlrQ?p=preview
 */
(function () {
    var app = angular.module('nnUtils');

    app.directive('multiswitchWhen', function () {
        return {
            transclude: 'element',
            priority: 800,
            require: '^ngSwitch',
            link: function (scope, element, attrs, ctrl, $transclude) {
                var selectTransclude = {transclude: $transclude, element: element};
                angular.forEach(attrs.multiswitchWhen.split('|'), function (switchWhen) {
                    ctrl.cases['!' + switchWhen] = (ctrl.cases['!' + switchWhen] || []);
                    ctrl.cases['!' + switchWhen].push(selectTransclude);
                });
            }
        }
    });

})();
