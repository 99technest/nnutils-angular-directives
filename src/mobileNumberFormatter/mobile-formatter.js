(function () {
    var app = angular.module('nnUtils');

    app.directive('mobileFormatter', ['$parse',function ($parse) {
        return {
            restrict: "A",
            template: '<input >{{result}}</input>',
            scope: {
                /* format: '@',*/
                countryCode: '@',
                separator: '@'
            },
            require: 'ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                scope.internalSeparator = '#';
                //scope.separator = angular.isUndefined(scope.separator) ?  '-' : scope.separator;
                scope.format = attrs.mobileFormatter;
                scope.element = element;
                scope.attrs = attrs;
                scope.ngModelCtrl = ngModelCtrl;
                //ngModelCtrl.$formatters.push( scope.myController.formatFor );

                scope.$watch(
                    function () {
                        return ngModelCtrl.$viewValue
                    },
                    function (newVal, oldVal) {
                        if (newVal === scope.lastResult) {
                            return;
                        }

                        scope.prop = ngModelCtrl.$modelValue;
                        var sep = angular.isUndefined(scope.separator) ? '-' : scope.separator;
                        if (sep === 'space') {
                            sep = ' ';
                        }

                        if (isUndefinedOrNull(newVal)) {
                            return;
                        }

                        var newValTransformed = replaceAllFound(newVal, sep, scope.internalSeparator);
                        oldVal = replaceAllFound(oldVal, sep, scope.internalSeparator);
                        var res = scope.myController.reEvaluate(newValTransformed, oldVal);
                        //ngModelCtrl.$setViewValue(res);

                        //put back the real seperator
                        res = replaceAllFound(res, scope.internalSeparator, sep);

                        ngModelCtrl.$viewValue = res;
                        ngModelCtrl.$render();

                    }
                )
            },
            controller: ['$scope', function ($scope) {
                $scope.myController = this;
                $scope.lastResult = {};

                this.reEvaluate = function (newVal, oldValue) {
                    var result = newVal;
                    if (isUndefinedOrNull(newVal) == false && isUndefinedOrNull($scope.format) == false) {
                        result = this.formatFor(newVal, oldValue);
                    }


                    $scope.lastResult = result;
                    return result;
                    //$scope.ngModelCtrl.$parsers.push(result);
                }

                this.getIndices = function () {
                    var indices = [];
                    for (var i = 0; i < $scope.format.length; i++) {
                        //dont check with $scope.separator. Let user always define it as xx-xxx...
                        if ($scope.format[i] === "-") {
                            indices.push(i);
                        }
                    }
                    return indices;
                }

                this.removeCountryCode = function (input) {
                    if (isUndefinedOrNull(input)) {
                        return input;
                    }

                    if (isUndefinedOrNull($scope.countryCode)) {
                        return input;
                    }
                    if (input.indexOf($scope.countryCode) == 0) {
                        return input.substring($scope.countryCode.length);
                    }
                    return input;
                }

                this.isEndingWithSeparator = function (input) {
                    if (isUndefinedOrNull(input) == false) {
                        return input[input.length - 1] === $scope.internalSeparator;
                    }
                    return false;
                }

                this.isbackSpacePerformed = function (newVal, oldValue) {
                    if (isUndefinedOrNull(newVal, oldValue) == false) {
                        if (oldValue.length == (newVal.length + 1)) {
                            if ((oldValue.substring(0, oldValue.length - 1)) === newVal) {
                                return true;
                            }
                        }
                    }
                    //not a backspace hit
                    return false;

                }

                this.formatFor = function (newVal, oldValue) {
                    var text = this.removeCountryCode(newVal);
                    var isBackSapce = this.isbackSpacePerformed(newVal, oldValue);

                    if (isUndefinedOrNull(oldValue) == false && isBackSapce && this.isEndingWithSeparator(oldValue)) {
                        //if backspace is hit when text is +91-00- then this block will ensure that processed text is +91-00
                        text = text.substring(0, text.length - 1);
                    }

                    if (isUndefinedOrNull($scope.countryCode, oldValue, newVal) == false) {
                        if (oldValue === $scope.countryCode && isBackSapce == true) {
                            //case when user hits back space when only country code is displayed
                            //intension would be to then clear country code. So returning empty string
                            return "";
                        }
                    }


                    var indices = this.getIndices();
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    //add the separator when its time
                    for (var i = 0; i < indices.length; i++) {
                        var inx = indices[i] - 1;
                        var nxt = inx + 1;

                        if (transformedInput.length > inx) {
                            transformedInput = transformedInput.substring(0, nxt) + $scope.internalSeparator
                                + transformedInput.substring(nxt, transformedInput.length);
                        }
                    }

                    var desiredLength = $scope.format.length;
                    if (isUndefinedOrNull($scope.countryCode) == false) {
                        transformedInput = $scope.countryCode + transformedInput;
                        desiredLength += $scope.countryCode.length;
                    }

                    if (transformedInput.length > desiredLength) {
                        transformedInput = transformedInput.substring(0, desiredLength);
                    }

                    if (isUndefinedOrNull(transformedInput, oldValue) == false && transformedInput.length < oldValue.length) {
                        if (this.isEndingWithSeparator(transformedInput)) {
                            // to support backspace with current text is ending as
                            // example : 123-
                            transformedInput = transformedInput.substring(0, transformedInput.length - 1);//
                        }
                    }
                    return transformedInput;

                }


            }]
        };
    }]);

    isUndefinedOrNull = function () {
        for (var i = 0; i < arguments.length; i++) {
            var val = arguments[i];
            var res = angular.isUndefined(val) || val === null || val == "";
            if (res)
                return true;
        }
        return false;
    };

    replaceAllFound = function (input, search, newChar) {
        if (isUndefinedOrNull(input))
            return input;

        return input.replace(new RegExp(search, 'g'), newChar);
    }

})();