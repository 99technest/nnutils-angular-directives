# What do I get here?
Here you will see many utility and functional libraries developed by 99technest.

# How to build:
Down and navigate to the downloaded directory. Perform below steps:
- ```npm install``` : This will install the dependencies defined in package.json(like bower and grunt).
- ```grunt``` : Just calling grunt is sufficient to run the gruntfile. This will package all your directives and put it in dist as minified and source.

# How to release updates
Just create a new tag once you are done with all the changes. Bower will automatically detect the new tag and create a new version.
Check [this video tutorial](https://youtu.be/UmewrWkulIg?list=PLbqNxCEdRFAODpM9u2ZK_9B7YlTP8LlUv&t=281).

# How to work in development environemnt
I have recorded a video for this. [Here is the link](https://youtu.be/SX24tfVUtBA?list=PLbqNxCEdRFAODpM9u2ZK_9B7YlTP8LlUv)

