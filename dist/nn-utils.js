
var app = angular.module('nnUtils', []);

/**
 * Created by Sandeep on 4/24/2016.
 */

(function () {
    var app = angular.module("nnUtils");
    app.directive('nnIcon', function () {
        return {
            restrict: "AE",
            scope: {
                successIcon: '@',
                failIcon: '@',
                warningIcon: '@',
                iconExternallyManaged: '@'
            },
            require: "^nnFormGroup",
            link: function (scope, element, attrs, formGroupController) {

                scope.element = element;
                //scope.iconExternallyManaged = attrs.nnIcon;

                if (!scope.iconExternallyManaged) {
                    scope.element.addClass("glyphicon form-control-feedback");
                }

                var controller = element.controller('nnIcon');//element.parent.controller('nnFormGroup');
                formGroupController.setIconController(controller);
            },
            controller: ['$scope', function ($scope) {
                this.setResult = function (errorMessage, result) {
                    //update class of scope.element
                    var success = $scope.successIcon ? $scope.successIcon : 'glyphicon-ok';
                    var fail = $scope.failIcon ? $scope.failIcon : 'glyphicon-remove';
                    var warning = $scope.warningIcon ? $scope.warningIcon : 'glyphicon-warning-sign';

                    if (isFormValidationSuccess(result)) {
                        $scope.element.addClass(success);
                        $scope.element.removeClass(fail);
                        $scope.element.removeClass(warning);
                    } else if (isFormValidationFailed(result)) {
                        $scope.element.addClass(fail);
                        $scope.element.removeClass(success);
                        $scope.element.removeClass(warning);
                    } else if (isFormValidationWarning(result)) {
                        $scope.element.addClass(warning);
                        $scope.element.removeClass(success);
                        $scope.element.removeClass(fail);
                    }
                }

                this.setReadOnlyMode = function (isReadOnly) {
                    if ($scope.iconExternallyManaged) {
                        return;
                    }
                    if (isReadOnly) {
                        $scope.element.addClass('hidden');
                    } else {
                        $scope.element.removeClass('hidden');
                    }
                }
            }]
        };
    });

    app.directive('nnFeedbackMessage', function () {
        return {
            restrict: "AE",
            template: '<div ng-class="desiredClass" ng-show="result">{{result}}</div>',
            scope: {},
            require: "^nnFormGroup",
            link: function (scope, element, attrs, formGroupController) {
                scope.element = element;
                scope.formGroupController = formGroupController;
                var controller = element.controller('nnFeedbackMessage');//element.parent.controller('nnFormGroup');
                formGroupController.setMessageController(controller);
            },
            controller: ['$scope', function ($scope) {
                this.setResult = function (errorMessage, result) {
                    if (isFormValidationSuccess(result)) {
                        $scope.result = null;
                        return;
                    } else if (isFormValidationFailed(result)) {
                        $scope.desiredClass = "text text-danger";
                    } else if (isFormValidationWarning(result)) {
                        $scope.desiredClass = "text text-warning";
                    }
                    $scope.result = errorMessage;
                }
                this.setReadOnlyMode = function (isReadOnly) {
                    if (isReadOnly) {
                        $scope.element.addClass('hidden');
                    } else {
                        $scope.element.removeClass('hidden');
                    }
                }
            }]
        };
    });

    app.directive('nnInput', function () {
        return {
            restrict: "AE",
            /*scope: {}, to ensure it does not clash with other directives, isolated scope should not exist here*/
            require: ['^nnFormGroup', '?ngModel'],
            link: function (scope, element, attrs, ctrls) {
                //scope.element = element; never copy element to scope as it not isolated scope
                var controller = element.controller('nnInput');
                var formGroupController = ctrls[0];
                formGroupController.setClientScope(scope);

                scope.touchedFunc = function () {

                    formGroupController.touched = true;
                    //dont try to over simplify below call. It is ok to do getCurrentModelValue here.
                    formGroupController.reEvaluateFor(controller.latestValue);
                    scope.$apply();
                }

                //scope.myController.init();
                scope.eval = function (newValue, oldValue) {
                    controller.latestValue = newValue;
                    //console.log( "ngModel value changed (A):", newValue );
                    if (newValue != oldValue) {
                        //when used as collection editor, validation should trigger when user moved from 1 object to other.
                        formGroupController.setNewModelValue(newValue);
                    }
                };


                formGroupController.setInputController(controller);
                controller.setElement(element);
                if (attrs.nnInput) {
                    //if ngModel is not used. developer can use nn-input='selectedItem' if ng-model='selectedItem' is not possible
                    formGroupController.modelName = attrs.nnInput;
                    scope.$watch(attrs.nnInput, scope.eval);
                    //just trying to re-evaluate on click for an unknown control. Anyways it will be triggered on value change
                    element.bind('click', scope.touchedFunc);

                } else {
                    //if input control is binded using ngModel.
                    formGroupController.modelName = attrs.ngModel;
                    //controller.latestValue = scope[attrs.ngModel];
                    scope.$watch(attrs.ngModel, scope.eval);
                    element.bind('focusout', scope.touchedFunc);
                    element.addClass('form-control');//dont add this binded control is not input (ex: custom dropdown button)
                }

            },
            controller: ['$scope',function ($scope) {
                //$scope.myController = this; not an isolated scope. so dont copy anything to $scope

                this.setElement = function (element) {
                    this.element = element;

                }
                this.setReadOnlyMode = function (isReadOnly) {
                    if (isReadOnly) {
                        this.element.addClass('hidden');
                    } else {
                        this.element.removeClass('hidden');
                    }
                }
            }]
        };
    });

    app.directive('nnDisplayOnly', function () {
        return {
            restrict: "AE",
            /*scope: {}, to ensure it does not clash with other directives, isolated scope should not exist here*/
            require: ['^nnFormGroup'],
            link: function (scope, element, attrs, ctrls) {
                //scope.element = element; never copy element to scope as it not isolated scope

                var formGroupController = ctrls[0];

                element.addClass('form-control');
                var controller = element.controller('nnDisplayOnly');
                formGroupController.setReadonlyElementController(controller);
                controller.setElement(element);
            },
            controller: ['$scope', function ($scope) {
                $scope.myController = this;
                this.init = function () {

                }
                this.setElement = function (element) {
                    this.element = element;

                }
                this.setReadOnlyMode = function (isReadOnly) {
                    //show this control in readonly mode only
                    if (isReadOnly) {
                        this.element.removeClass('hidden');
                    } else {
                        this.element.addClass('hidden');
                    }
                }
            }]
        };
    });

    app.directive('nnFormGroup', function () {
        return {
            restrict: "AE",
            scope: {
                errorCheck: '&',
                warningCheck: '&',
                errorIf: '@', /*values can be email,empty and more. look below*/
                warningIf: '@'
            },
            require: "^nnForm",
            link: function (scope, element, attrs, parentFormCtrl) {
                scope.element = element;
                scope.attrs = attrs;
                parentFormCtrl.registerModel(scope.myController.modelName);
                scope.parentFormCtrl = parentFormCtrl;
                element.addClass("form-group has-feedback");
                parentFormCtrl.registerFormGroup(scope.myController);
            },
            /*transclude: 'element',
             terminal: true,*/
            controller: ['$scope', function ($scope) {
                $scope.myController = this;
                this.touched = false;
                this.canShowFeedback = true;
                $scope.isEvaluated = false;
                this.clientScope = null;//will be updated by nnInput
                this.asyncErrorMsg = null;//will be set by client through nnForm controller
                //client scope is the scope(or $ctrl) written by consumer of nnFormValidator directive.
                this.setClientScope = function (clientScope) {
                    this.clientScope = clientScope;
                }

                this.setAsyncError = function (newValue) {
                    $scope.myController.asyncErrorMsg = newValue;
                    //calling as $scope.myController boz 'this' keyword doesnt work within watch
                    $scope.myController.reEvaluateFor($scope.myController.getCurrentModelValue());
                }


                this.getCurrentModelValue = function () {
                    /*var prts = this.modelName.split('.');
                     var tryGetValue = this.clientScope[prts[0]];
                     for (var i = 1; i < prts.length; i++) {
                     tryGetValue = tryGetValue[prts[i]];
                     }
                     return tryGetValue;*/
                    return $scope.myController.ngModelResolved;
                }

                this.setReadOnlyMode = function (newVal) {
                    if ($scope.iconController) {
                        $scope.iconController.setReadOnlyMode(newVal);
                    }
                    if ($scope.messageController) {
                        $scope.messageController.setReadOnlyMode(newVal);
                    }
                    if ($scope.nnInputController) {
                        $scope.nnInputController.setReadOnlyMode(newVal);
                    }

                    if ($scope.displayOnlyElementController) {
                        $scope.displayOnlyElementController.setReadOnlyMode(newVal);
                    }

                }

                this.showFeedback = function (canShow) {
                    this.canShowFeedback = canShow;
                    if ($scope.isEvaluated) {
                        this.reEvaluate();
                    }
                }

                this.getError = function (modelValue) {
                    //var labelName = getLabel($scope.element);
                    //first check on predefined built in validationsd offered by this directive
                    if (angular.isUndefined($scope.errorIf) == false) {
                        var op = new validationOptions(modelValue, $scope.errorIf);
                        var opr = op.validate();
                        if (opr != null) {
                            return opr;
                        }
                    }

                    //asyncError is the property which holds the actual value (asyncErrorMsg)
                    if (this.asyncErrorMsg) {
                        return this.asyncErrorMsg;
                    }

                    return $scope.errorCheck();
                };

                this.getWarning = function (modelValue) {
                    //var labelName = getLabel($scope.element);
                    //first check on predefined built in validationsd offered by this directive
                    if (angular.isUndefined($scope.warningIf) == false) {
                        var op = new validationOptions(modelValue, $scope.warningIf);
                        var opr = op.validateForWarning();
                        if (opr != null) {
                            return opr;
                        }
                    }
                    return $scope.warningCheck();
                };

                this.setResult = function (errorMessage, result) {
                    //update class of scope.element
                    //Should look like  form-group has-success has-feedback

                    var success = 'has-success';
                    var fail = 'has-error';
                    var warning = 'has-warning';

                    $scope.element.removeClass(success);
                    $scope.element.removeClass(fail);
                    $scope.element.removeClass(warning);
                    if (this.canShowFeedback) {
                        if (isFormValidationSuccess(result)) {
                            $scope.element.addClass(success);
                        } else if (isFormValidationFailed(result)) {
                            $scope.element.addClass(fail);
                        } else if (isFormValidationWarning(result)) {
                            $scope.element.addClass(warning);
                        }
                    }

                    var labelName = getLabel($scope.element);
                    if (!isUndefinedOrNull($scope.parentFormCtrl)) {
                        $scope.parentFormCtrl.setResult(labelName, $scope.myController.modelName, errorMessage, result);
                    }
                    if (!isUndefinedOrNull($scope.iconController)) {
                        $scope.iconController.setResult(errorMessage, result);
                    }
                    if (!isUndefinedOrNull($scope.messageController)) {
                        $scope.messageController.setResult(errorMessage, result);
                    }

                };

                this.setInputController = function (nnInputCtrl) {
                    $scope.nnInputController = nnInputCtrl;
                }

                this.setReadonlyElementController = function (ctrl) {
                    $scope.displayOnlyElementController = ctrl;
                }

                this.setIconController = function (iconCntrl) {
                    $scope.iconController = iconCntrl;
                };

                this.setMessageController = function (msgCntrl) {
                    $scope.messageController = msgCntrl;
                };

                this.reEvaluateFor = function (value) {
                    $scope.myController.ngModelResolved = value;
                    this.reEvaluate();
                }

                this.setNewModelValue = function (value) {
                    $scope.myController.ngModelResolved = value;
                    //dont call reEvaluate as it will check for touched. When model itself changes, it is required to invalidate
                    this.invalidate();
                }

                this.reEvaluate = function () {
                    if ($scope.myController.touched == false) {
                        return;
                    }
                    this.invalidate();
                }

                this.invalidate = function () {

                    $scope.isEvaluated = true;
                    //below is for your reference
                    //console.log($scope.attrs.model + " changed to " + $scope.modelName);

                    var errMsg = $scope.myController.getError($scope.myController.ngModelResolved);
                    if (isUndefinedOrNull(errMsg) == false) {
                        $scope.myController.setResult(errMsg, "Failed");
                    } else {
                        var warningMsg = $scope.myController.getWarning($scope.myController.ngModelResolved);
                        if (isUndefinedOrNull(warningMsg) == false) {
                            $scope.myController.setResult(warningMsg, "Warning");
                        } else {
                            $scope.myController.setResult(null, "Success");
                        }

                    }
                }

            }]
        };
    });

    app.directive('nnForm', function () {
        return {
            restrict: "AE",
            /*template: '<div class="form-horizontal" role="form"></div>',*/
            scope: {
                result: '=',
                showFeedback: "=",
                readOnlyMode: "=",
                hasDynamicFields: "="
            },
            link: function (scope, element, attrs, parentFormCtrl) {
                scope.element = element;

                //allow client to invalidate error status at his will
                scope.result(scope.myResult);//scope.myResult is set by controller

                /*it has to be in link as groups would not have loaded if this code goes in controller*/
                if (scope.hasDynamicFields) {
                    scope.myResult.formController.refreshLayout();
                }
            },
            controller: ['$scope', function ($scope) {
                $scope.myResult = new ValidationResult();
                $scope.myResult.formController = this;

                this.setResult = function (labelName, variableName, errorMessage, isPassed) {
                    if (isUndefinedOrNull($scope.myResult)) {
                        $scope.myResult = new ValidationResult();
                    }
                    $scope.myResult.errorMap[variableName] = new EachFormResult(labelName, variableName, errorMessage, isPassed);
                    $scope.result($scope.myResult);
                }

                this.registerModel = function (modelName) {
                    $scope.myResult.registeredModels.push(modelName);
                }

                this.registerFormGroup = function (childGroup) {
                    if (isUndefinedOrNull($scope.childgroups)) {
                        $scope.childgroups = [];
                    }
                    $scope.childgroups.push(childGroup);

                    if ($scope.hasDynamicFields) {
                        childGroup.showFeedback($scope.showFeedback);
                        childGroup.setReadOnlyMode($scope.readOnlyMode);
                    }
                }

                this.asyncError = function (modelName, asyncRes) {
                    for (var i = 0; i < $scope.childgroups.length; i++) {
                        var grpController = $scope.childgroups[i];
                        if (grpController.modelName === modelName) {
                            grpController.setAsyncError(asyncRes);
                        }
                    }
                }

                this.invalidateAll = function () {
                    console.log("invalidate called with items count=" + $scope.childgroups.length);
                    for (var i = 0; i < $scope.childgroups.length; i++) {
                        $scope.childgroups[i].invalidate();
                    }
                }

                this.refreshFeedback = function (isShowFeedback) {
                    angular.forEach($scope.childgroups, function (formGroup) {
                        formGroup.showFeedback(isShowFeedback);
                    });
                }
                $scope.$watch('showFeedback', this.refreshFeedback);

                this.refreshIsReadonlyMode = function (isReadonlyMode) {
                    angular.forEach($scope.childgroups, function (formGroup) {
                        formGroup.setReadOnlyMode(isReadonlyMode);
                    });
                }
                $scope.$watch('readOnlyMode', this.refreshIsReadonlyMode);

                this.refreshLayout = function () {
                    //call this function when an item is added dynamically
                    this.refreshFeedback($scope.showFeedback);
                    this.refreshIsReadonlyMode($scope.readOnlyMode);
                }
            }]
        };
    });

    function ValidationResult() {
        this.errorMap = {};
        this.registeredModels = [];
        this.formController;

        this.invalidateAll = function () {
            this.formController.invalidateAll();
        }
        this.asyncError = function (modelName, asyncRes) {
            this.formController.asyncError(modelName, asyncRes);
        }

        this.getErrorFieldNames = function () {
            var array = [];
            for (var key in this.errorMap) {
                if (isFormValidationFailed(this.errorMap[key].result)) {
                    array.push(this.errorMap[key].variableName);
                }
            }
            return array;
        }

        //returns field names which are yet to be entered by the user
        this.getUnValidatedFields = function () {
            var result = [];
            for (var i = 0; i < this.registeredModels.length; i++) {
                var key = this.registeredModels[i];
                if (hasOwnProperty.call(this.errorMap, key) == false) {
                    if (isUndefinedOrNull(key) == false) {
                        result.push(key);
                    }
                }
            }
            return result;
        }

        this.isAllSuccessExcept = function (excepts) {
            for (var i = 0; i < this.registeredModels.length; i++) {
                var key = this.registeredModels[i];
                if (isUndefinedOrNull(key))
                    continue;

                if (excepts != null && excepts.indexOf(key) == -1) {
                    if (isUndefinedOrNull(this.errorMap[key])) {
                        return false;//it means this field was never validated. Possible when user had not even entered once.
                    }
                    if (isFormValidationFailed(this.errorMap[key].result)) {
                        return false;
                    }
                }
            }
            return true;
        }

        this.isAllSuccess = function () {
            return this.isAllSuccessExcept(null);
        }

        this.getErrorFieldDisplayNames = function () {
            var array = [];
            for (var key in this.errorMap) {
                if (isFormValidationFailed(this.errorMap[key].result)) {
                    array.push(this.errorMap[key].getDisplayName());
                }
            }
            return array;
        }

        this.getErrors = function () {
            var array = [];
            for (var key in this.errorMap) {
                if (isFormValidationFailed(this.errorMap[key].result)) {
                    array.push(this.errorMap[key]);
                }
            }
            return array;
        }

        this.getWarnings = function () {
            var array = [];
            for (var key in this.errorMap) {
                if (isFormValidationWarning(this.errorMap[key].result)) {
                    array.push(this.errorMap[key]);
                }
            }
            return array;
        }

        this.getResultDetails = function () {
            var array = [];
            for (var key in this.errorMap) {
                array.push(this.errorMap[key]);
            }
            return array;
        }

        this.hasError = function (fieldName) {
            if (isUndefinedOrNull(this.errorMap))
                return null;

            if (isUndefinedOrNull(this.errorMap[fieldName]))
                return null;
            return !this.errorMap[fieldName].result;
        }

        this.resultOf = function (fieldName) {
            if (isUndefinedOrNull(this.errorMap))
                return null;

            if (isUndefinedOrNull(this.errorMap[fieldName]))
                return null;
            return !this.errorMap[fieldName];
        }
    }

    function EachFormResult(labelName, variableName, errMsg, result) {
        this.labelName = labelName;
        this.variableName = variableName;
        this.errorMessage = errMsg;
        this.result = result;
        this.getDisplayName = function () {
            if (labelName === null) {
                return variableName;
            } else {
                return labelName;
            }
        }
    }

    var isUndefinedOrNull = function (val) {
        return angular.isUndefined(val) || val === null || val == "";
    }

    var getLabel = function (element) {
        var res = element[0].querySelector('.control-label');
        if (res === null) {
            return null;
        }
        return res.innerText;
    }

    var validationOptions = function (value, optedRules) {
        this.value = value;
        this.optedRules = optedRules;//email,empty

        this.validateForWarning = function () {
            var res = this.validate();
            if (isUndefinedOrNull(res) == false) {
                return res.replace("cannot be", "is").replace("has to be", "can be");
            }
            return res;
        }

        this.validate = function () {
            var partsOfStr = this.optedRules.split(',');

            for (i = 0; i < partsOfStr.length; i++) {
                var rule = partsOfStr[i];
                if (rule === 'empty') {
                    var msg = "value cannot be empty";
                    if (this.value == null) {
                        return msg;
                    }
                    if (this.value === '') {
                        return msg;
                    }
                }
                if (rule === 'url') {
                    if (this.validateUrl() == false)
                        return "Invalid URL";
                }
                if (rule === 'email') {
                    if (this.validateEmail() == false)
                        return "Invalid email address";
                }
                if (rule === 'number') {
                    if (this.isNumeric() == false)
                        return "Invalid number";
                }
                if (rule.startsWith('min-length-')) {
                    var desired = rule.split('min-length-')[1];
                    var errMsg = "Minimum length has to be " + desired;
                    if (!this.value)
                        return errMsg;

                    if (this.value.length < desired)
                        return errMsg;
                }
                if (rule.startsWith('max-length-')) {
                    var desired = rule.split('max-length-')[1];
                    var errMsg = "Maximum length has to be " + desired;
                    if (!this.value)
                        return errMsg;

                    if (this.value.length > desired)
                        return "Maximum length has to be " + desired;
                }


            }
            return null;

        }

        this.validateEmail = function () {
            var atpos = value.indexOf("@");
            var dotpos = value.lastIndexOf(".");
            if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= value.length) {
                return false;
            }
        }

        this.validateUrl = function () {
            var urlregex = new RegExp("^(http|https|ftp)\://([a-zA-Z0-9\.\-]+(\:[a-zA-Z0-9\.&amp;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(\:[0-9]+)*(/($|[a-zA-Z0-9\.\,\?\'\\\+&amp;%\$#\=~_\-]+))*$");
            return urlregex.test(value);
            //return false;
        }

        this.isNumeric = function () {
            return !isNaN(parseFloat(value)) && isFinite(value);
        }


    }

    var isFormValidationSuccess = function (val) {
        if (val === "Success")
            return true;
        else return false;
    }

    var isFormValidationFailed = function (result) {
        if (result === "Failed")
            return true;
        else return false;
    }

    var isFormValidationWarning = function (val) {
        if (val === "Warning")
            return true;
        else return false;
    }

})();

(function () {
    var app = angular.module('nnUtils');

    app.directive('mobileFormatter', ['$parse',function ($parse) {
        return {
            restrict: "A",
            template: '<input >{{result}}</input>',
            scope: {
                /* format: '@',*/
                countryCode: '@',
                separator: '@'
            },
            require: 'ngModel',
            link: function (scope, element, attrs, ngModelCtrl) {
                scope.internalSeparator = '#';
                //scope.separator = angular.isUndefined(scope.separator) ?  '-' : scope.separator;
                scope.format = attrs.mobileFormatter;
                scope.element = element;
                scope.attrs = attrs;
                scope.ngModelCtrl = ngModelCtrl;
                //ngModelCtrl.$formatters.push( scope.myController.formatFor );

                scope.$watch(
                    function () {
                        return ngModelCtrl.$viewValue
                    },
                    function (newVal, oldVal) {
                        if (newVal === scope.lastResult) {
                            return;
                        }

                        scope.prop = ngModelCtrl.$modelValue;
                        var sep = angular.isUndefined(scope.separator) ? '-' : scope.separator;
                        if (sep === 'space') {
                            sep = ' ';
                        }

                        if (isUndefinedOrNull(newVal)) {
                            return;
                        }

                        var newValTransformed = replaceAllFound(newVal, sep, scope.internalSeparator);
                        oldVal = replaceAllFound(oldVal, sep, scope.internalSeparator);
                        var res = scope.myController.reEvaluate(newValTransformed, oldVal);
                        //ngModelCtrl.$setViewValue(res);

                        //put back the real seperator
                        res = replaceAllFound(res, scope.internalSeparator, sep);

                        ngModelCtrl.$viewValue = res;
                        ngModelCtrl.$render();

                    }
                )
            },
            controller: ['$scope', function ($scope) {
                $scope.myController = this;
                $scope.lastResult = {};

                this.reEvaluate = function (newVal, oldValue) {
                    var result = newVal;
                    if (isUndefinedOrNull(newVal) == false && isUndefinedOrNull($scope.format) == false) {
                        result = this.formatFor(newVal, oldValue);
                    }


                    $scope.lastResult = result;
                    return result;
                    //$scope.ngModelCtrl.$parsers.push(result);
                }

                this.getIndices = function () {
                    var indices = [];
                    for (var i = 0; i < $scope.format.length; i++) {
                        //dont check with $scope.separator. Let user always define it as xx-xxx...
                        if ($scope.format[i] === "-") {
                            indices.push(i);
                        }
                    }
                    return indices;
                }

                this.removeCountryCode = function (input) {
                    if (isUndefinedOrNull(input)) {
                        return input;
                    }

                    if (isUndefinedOrNull($scope.countryCode)) {
                        return input;
                    }
                    if (input.indexOf($scope.countryCode) == 0) {
                        return input.substring($scope.countryCode.length);
                    }
                    return input;
                }

                this.isEndingWithSeparator = function (input) {
                    if (isUndefinedOrNull(input) == false) {
                        return input[input.length - 1] === $scope.internalSeparator;
                    }
                    return false;
                }

                this.isbackSpacePerformed = function (newVal, oldValue) {
                    if (isUndefinedOrNull(newVal, oldValue) == false) {
                        if (oldValue.length == (newVal.length + 1)) {
                            if ((oldValue.substring(0, oldValue.length - 1)) === newVal) {
                                return true;
                            }
                        }
                    }
                    //not a backspace hit
                    return false;

                }

                this.formatFor = function (newVal, oldValue) {
                    var text = this.removeCountryCode(newVal);
                    var isBackSapce = this.isbackSpacePerformed(newVal, oldValue);

                    if (isUndefinedOrNull(oldValue) == false && isBackSapce && this.isEndingWithSeparator(oldValue)) {
                        //if backspace is hit when text is +91-00- then this block will ensure that processed text is +91-00
                        text = text.substring(0, text.length - 1);
                    }

                    if (isUndefinedOrNull($scope.countryCode, oldValue, newVal) == false) {
                        if (oldValue === $scope.countryCode && isBackSapce == true) {
                            //case when user hits back space when only country code is displayed
                            //intension would be to then clear country code. So returning empty string
                            return "";
                        }
                    }


                    var indices = this.getIndices();
                    var transformedInput = text.replace(/[^0-9]/g, '');

                    //add the separator when its time
                    for (var i = 0; i < indices.length; i++) {
                        var inx = indices[i] - 1;
                        var nxt = inx + 1;

                        if (transformedInput.length > inx) {
                            transformedInput = transformedInput.substring(0, nxt) + $scope.internalSeparator
                                + transformedInput.substring(nxt, transformedInput.length);
                        }
                    }

                    var desiredLength = $scope.format.length;
                    if (isUndefinedOrNull($scope.countryCode) == false) {
                        transformedInput = $scope.countryCode + transformedInput;
                        desiredLength += $scope.countryCode.length;
                    }

                    if (transformedInput.length > desiredLength) {
                        transformedInput = transformedInput.substring(0, desiredLength);
                    }

                    if (isUndefinedOrNull(transformedInput, oldValue) == false && transformedInput.length < oldValue.length) {
                        if (this.isEndingWithSeparator(transformedInput)) {
                            // to support backspace with current text is ending as
                            // example : 123-
                            transformedInput = transformedInput.substring(0, transformedInput.length - 1);//
                        }
                    }
                    return transformedInput;

                }


            }]
        };
    }]);

    isUndefinedOrNull = function () {
        for (var i = 0; i < arguments.length; i++) {
            var val = arguments[i];
            var res = angular.isUndefined(val) || val === null || val == "";
            if (res)
                return true;
        }
        return false;
    };

    replaceAllFound = function (input, search, newChar) {
        if (isUndefinedOrNull(input))
            return input;

        return input.replace(new RegExp(search, 'g'), newChar);
    }

})();
angular.module('nnUtils').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('src/thermo-meter/Termometer.html',
    "<div ng-style=\"mainStyle\" class=\"tg-thermometer\" ng-class=\"size\">\r" +
    "\n" +
    "	<div class=\"draw-a\"></div>\r" +
    "\n" +
    "	<div class=\"draw-b\"></div>\r" +
    "\n" +
    "	<div class=\"meter\">\r" +
    "\n" +
    "		<div class=\"statistics\" style=\"margin-left: 8px\">\r" +
    "\n" +
    "			<div class=\"percent percent-a\"> {{maxVal}} {{unit}}</div>\r" +
    "\n" +
    "			<div class=\"percent percent-b\"> {{p75}} {{unit}}</div>\r" +
    "\n" +
    "			<div class=\"percent percent-c\"> {{p50}} {{unit}}</div>\r" +
    "\n" +
    "			<div class=\"percent percent-d\"> {{p25}} {{unit}}</div>\r" +
    "\n" +
    "			<div class=\"percent percent-e\"> {{minVal}} {{unit}}</div>\r" +
    "\n" +
    "		</div>\r" +
    "\n" +
    "		<div ng-style=\"mercuryStyle\" class=\"mercury\">\r" +
    "\n" +
    "			<div class=\"percent-current\">{{displayVal}} {{unit}}</div>\r" +
    "\n" +
    "			<div class=\"mask\">\r" +
    "\n" +
    "				<div class=\"bg-color\" ng-style=\"maskStype\"></div>\r" +
    "\n" +
    "			</div>\r" +
    "\n" +
    "		</div>\r" +
    "\n" +
    "	</div>\r" +
    "\n" +
    "</div>"
  );

}]);

(function () {
angular.module('nnUtils').directive('thermometer', ['$window', function($window) {

  return {
    restrict: 'E',
    scope: true,
    require: 'ngModel',
    templateUrl: 'src/thermo-meter/Termometer.html',
    link: function(scope, elem, attrs, ngModelCtrl) {

      scope.size = attrs.size;
      scope.height = attrs.height;
      scope.percent = attrs.percent;
      scope.ngModelCtrl = ngModelCtrl;
      scope.unit = attrs.unit;
      scope.minVal = attrs.minVal;
      scope.maxVal = attrs.maxVal;

      scope.mainStyle = {height: scope.height + "px"};
      scope.maskStype = {height: (scope.height - 53) + "px"};
      
      scope.refreshMercury = function(){
        scope.mercuryStyle = {height: scope.percent + "%"};
      }
      
      scope.calculatePercentage = function(min, max, value) {
        if (min < 0) {
          var delta = 0 - min;
          min += delta;
          max += delta;
          value += delta;
        }
        scope.p75 = (max * 75) / 100;
        scope.p50 = (max * 50) / 100;
        scope.p25 = (max * 25) / 100;
        
        return (100 * value) / max;
      }

      scope.$watch(function() {
        return ngModelCtrl.$viewValue;
      }, function(newVal, oldVal) {
        if (newVal === scope.percent) { return; }
        if (!scope.minVal && !scope.maxVal) {
          scope.minVal = 0;
          scope.maxVal = 100;
          scope.percent = newVal;
        }
        scope.percent = scope.calculatePercentage(parseFloat(scope.minVal), parseFloat(scope.maxVal), parseFloat(newVal));
        scope.displayVal = newVal;
        scope.refreshMercury();
      })
    }
  };

}]);
})();